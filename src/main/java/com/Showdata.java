package com;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class Showdata {
	
	public static void main(String[] args) {
		
		Resource resource=new ClassPathResource("spconfig.xml");
		BeanFactory factory=new XmlBeanFactory(resource);
		
		Object obj=factory.getBean("id1");
		
		Bean bean=(Bean)obj;
		bean.show();
				
		
		
		Object obj2=factory.getBean("id2");
		
		Bean bean2=(Bean)obj2;
		bean2.show();
		
		bean.show();
		
		
		
		
	}

}
